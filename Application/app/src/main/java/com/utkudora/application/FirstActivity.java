package com.utkudora.application;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class FirstActivity extends AppCompatActivity implements View.OnClickListener {
    private Timer myTimer;
    final Handler handler = new Handler();
int score = 0;
    private void UpdateGUI() {


        handler.post(runnable);
    }

    Button button ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
       // handler.postDelayed(runnable, 1000);
//startRandomButton();






    }
    final Runnable runnable = new Runnable() {
        public void run() {

            Random r = new Random();
            int i1 = r.nextInt(1000 - 65) + 65;

            Random r2 = new Random();
            int i2 = r2.nextInt(100 - 65) + 65;
            button.setX(i1);
            button.setY(i2);

      /* and here comes the "trick" */
            handler.postDelayed(this, 1000);
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startRandomButton() {

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Button button = (Button) findViewById(R.id.FruitButton);
                Random r = new Random();
            int i1 = r.nextInt(1000 - 65) + 65;

            Random r2 = new Random();
            int i2 = r2.nextInt(100 - 65) + 65;
                button.setX(i1);
                button.setY(i2);

            }

        }, 0, 3000);//Update button every second
    }



    //public void relocateButton(){
//    myTimer = new Timer();
//    myTimer.scheduleAtFixedRate(new TimerTask() {
//        @Override
//        public void run() {
//            // Method
//            Button button = (Button) findViewById(R.id.FruitButton);
//            Random r3 = new Random();
//            int i3 = r3.nextInt(4);
//            String array[] = {"Cherry", "Melon", "Banana", "Apple"};
//            button.setText(array[i3]);
//            Random r = new Random();
//            int i1 = r.nextInt(1000 - 65) + 65;
//
//            Random r2 = new Random();
//            int i2 = r2.nextInt(100 - 65) + 65;
//            button.setX(i1);
//            button.setY(i2);
//
//
//        }
//    }, 0, 500);
//
//
//
//}
    public static Point getDisplaySize(@NonNull Context context) {
        Point point = new Point();
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        manager.getDefaultDisplay().getSize(point);
        return point;
    }


    @Override
    public void onClick(View v) {

         button = (Button) v;
        if(button.getText()!="Throw Fruit"){
            score = score +5;
            TextView scoreLabel = (TextView) findViewById(R.id.textView);
            scoreLabel.setText("Score:  "+score);
        }
        Random r3 = new Random();
        int i3 = r3.nextInt(4);
        String array[]= {"Cherry","Melon","Banana","Apple"};
        ((Button) v).setText(array[i3]);
        Random r = new Random();
        if(button.getText()=="Banana") {
            button.setBackgroundColor(Color.YELLOW);
        }
        if(button.getText()=="Melon") {
            button.setBackgroundColor(Color.GREEN);
        }
        if(button.getText()=="Apple") {
            button.setBackgroundColor(Color.RED);
        }
        if(button.getText()=="Cherry") {
            button.setBackgroundColor(Color.BLUE);
        }
        int i1 = r.nextInt(1000 - 65) + 65;

        Random r2 = new Random();
        int i2 = r2.nextInt(200 - 65) + 65;

//        int randomX = new Random().nextInt(getDisplaySize(this).x-200)+100;
//        int randomY = new Random().nextInt(getDisplaySize(this).y-200)+100;
////
//        button.setX(randomX);
//        button.setY(randomY);

        button.setX(i1);
        button.setY(i2);

//
//        myTimer.schedule(new TimerTask() {
//            public void run() {
//                UpdateGUI();
//            }
//        }, 0, 1000);


        handler.postDelayed(new Runnable() {
           // Button button = (Button) findViewById(R.id.FruitButton);


            public void run() {

//                Random r = new Random();
//                int i1 = r.nextInt(1000 - 65) + 65;
//
//                Random r2 = new Random();
//                int i2 = r2.nextInt(200 - 65) + 65;

                    button.setBackgroundColor(Color.GRAY);

                button.setText("Throw Fruit");
                button.setX(400);
                button.setY(350);

            }
        }, 1000);



    }
}
